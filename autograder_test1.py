import unittest
from unittest import TextTestRunner, TextTestResult
from hw1 import *
import re


class TestHomework1(unittest.TestCase):

    def setUp(self):
        pass

    # fizzbuzz

    def test_fizzbuzz_name1_points2(self):
        self.assertEqual(fizzbuzz(1), None)

    def test_fizzbuzz_name3_points2(self):
        self.assertEqual(fizzbuzz(3), 'Fizz')

    def test_fizzbuzz_name5_points2(self):
        self.assertEqual(fizzbuzz(5), 'Buzz')

    def test_fizzbuzz_name14_points2(self):
        self.assertEqual(fizzbuzz(14), None)
    
    def test_fizzbuzz_name15_points2(self):
        self.assertEqual(fizzbuzz(15), 'FizzBuzz')

    # snapcrackle

    def test_snapcrackle_nameint_points3(self):
        self.assertEqual(snapcrackle(3), 'Snap')

    def test_snapcrackle_namefloat_points3(self):
        self.assertEqual(snapcrackle(3.0), 'Crackle')

    def test_snapcrackle_namecomplex_points2(self):
        self.assertEqual(snapcrackle(5j), None)

    def test_snapcrackle_namestring_points2(self):
        self.assertEqual(snapcrackle('junk'), None)

    # is_prime
    def test_is_prime_name1_points2(self):
        self.assertFalse(is_prime(1))
    
    def test_is_prime_name2_points2(self):
        self.assertTrue(is_prime(2))

    def test_is_prime_name3_points1(self):
        self.assertTrue(is_prime(3))
        
    def test_is_prime_name4_points2(self):
        self.assertFalse(is_prime(4))

    def test_is_prime_name23_points1(self):
        self.assertTrue(is_prime(23))
        
    def test_is_prime_name29_points1(self):
        self.assertTrue(is_prime(29))

    def test_is_prime_name46_points1(self):
        self.assertFalse(is_prime(46))

    # nth_prime
    def test_nth_prime_name1_points4(self):
        self.assertEquals(nth_prime(1), 2)

    def test_nth_prime_name5_points3(self):
        self.assertEquals(nth_prime(5), 11)

    def test_nth_prime_name10_points3(self):
        self.assertEquals(nth_prime(10), 29)

    # gcd iter
    def test_gcd_iter_name10_1_points3(self):
        self.assertEquals(gcd_iter(10, 1), 1)

    def test_gcd_iter_name23_14_points3(self):
        self.assertEquals(gcd_iter(23, 14), 1)

    def test_gcd_iter_name26_neg_13_points2(self):
        self.assertEquals(gcd_iter(26, -13), -13)
    
    def test_gcd_iter_name10_0_points2(self):
        self.assertEquals(gcd_iter(10, 0), 10)

    # gcd rec
    def test_gcd_rec_name10_1_points3(self):
        self.assertEquals(gcd_rec(10, 1), 1)

    def test_gcd_rec_name23_14_points3(self):
        self.assertEquals(gcd_rec(23, 14), 1)

    def test_gcd_rec_name26_neg_13_points2(self):
        self.assertEquals(gcd_rec(26, -13), -13)
    
    def test_gcd_rec_name10_0_points2(self):
        self.assertEquals(gcd_rec(10, 0), 10)

    # fib iter
    def test_fib_iter_name1_points3(self):
        self.assertEquals(fib_iter(1), 0)
        
    def test_fib_iter_name2_points3(self):
        self.assertEquals(fib_iter(2), 1)

    def test_fib_iter_name8_points2(self):
        self.assertEquals(fib_iter(8), 13)

    def test_fib_iter_name13_points2(self):
        self.assertEquals(fib_iter(13), 144)

    # fib rec
    def test_fib_rec_name1_points3(self):
        self.assertEquals(fib_rec(1), 0)
        
    def test_fib_rec_name2_points3(self):
        self.assertEquals(fib_rec(2), 1)

    def test_fib_rec_name8_points2(self):
        self.assertEquals(fib_rec(8), 13)

    def test_fib_rec_name13_points2(self):
        self.assertEquals(fib_rec(13), 144)
        
class MyTestResult(TextTestResult):

    def __init__(self, stream, descriptions, verbosity):
        super(MyTestResult, self).__init__(stream, descriptions, verbosity)
        self.successes = []


    def addSuccess(self,test):
        res = super(MyTestResult, self).addSuccess(test)
        self.successes.append(test)
        return res


def parse_test_name(test):
    regex = r'test_(?P<function>.*)_name(?P<name>.*)_points(?P<points>\d*)'
    m = re.search(regex, str(test))
    d = {k: m.group(k) for k in ['function', 'name', 'points']}
    d['points'] = int(d['points'])
    return d


def summary_string(good, bad):
    total = 0
    correct = 0
    functions = {x['function'] for x in good + bad}
    results = {x: {'total': 0, 'correct': 0, 'wrong': []}
               for x in functions}
    for test in good:
        record = results[test['function']]
        record['total'] += test['points']
        total += test['points']
        record['correct'] += test['points']
        correct += test['points']
    for test in bad:
        record = results[test['function']]
        record['total'] += test['points']
        total += test['points']
        record['wrong'].append(test)
    output = []
    for func, result in results.items():
        output.append('{}: {}/{}'.format(func, result['correct'], result['total']))
        output.extend(['    {}: -{}'.format(x['name'], x['points'])
                       for x in result['wrong']])
    output.append('total: {}/{}'.format(correct, total))
    return '\n'.join(output)

def main():
    with open('test_out.txt', 'w') as out:
        MyTestRunner = TextTestRunner(stream=out,
                                      resultclass=MyTestResult)
        res = unittest.main(exit=False,
                            testRunner=MyTestRunner)
    res = res.result
    errors = res.errors
    fails = res.failures
    bad = errors + fails
    bad = [parse_test_name(test) for (test, trace) in bad]
    good = list(map(parse_test_name, res.successes))
    summary_text = summary_string(good, bad)
    with open('test_summary.txt', 'w') as summary:
        summary.write(summary_text)

if __name__ == '__main__':
    main()
