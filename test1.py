import unittest
from hw1 import *

class TestHomework1(unittest.TestCase):

    def setUp(self):
        pass

    # fizzbuzz

    def test_fizzbuzz(self):
        self.assertEqual(fizzbuzz(3), 'Fizz')

    # snapcrackle

    def test_snapcrackle(self):
        self.assertEqual(snapcrackle(3), 'Snap')

    # is_prime
    def test_is_prime(self):
        self.assertFalse(is_prime(1))

    def test_is_prime_name2_points2(self):
        self.assertTrue(is_prime(2))

    # nth_prime
    def test_nth_prime(self):
        self.assertEquals(nth_prime(5), 11)

    # gcd iter
    def test_gcd_iter(self):
        self.assertEquals(gcd_iter(10, 1), 1)

    # gcd rec
    def test_gcd_rec(self):
        self.assertEquals(gcd_rec(10, 1), 1)

    # fib iter
    def test_fib_iter(self):
        self.assertEquals(fib_iter(8), 13)

    # fib rec
    def test_fib_rec(self):
        self.assertEquals(fib_rec(8), 13)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
