""" Homework 1
-- Due Monday, Jan. 25th at 23:59
-- Always write the final code yourself
-- Cite any websites you referenced
-- Use the PEP-8 checker for full style points:
https://pypi.python.org/pypi/pep8
"""


def fizzbuzz(n):
    """ If n is divisible by 3, return "Fizz"
    If n is divisible by 5, return "Buzz"
    If n is divisible by 3 and 5, return "FizzBuzz"
    Else, do nothing.
    """
    pass


def snapcrackle(n):
    """ If n is an int, return "Snap"
    If n is a float, return "Crackle"
    Else, do nothing.
    """
    pass


def is_prime(n):
    """ Return True if n is prime and False otherwise.
    Use this function to help write 'nth_prime' below.
    """
    pass


def nth_prime(n):
    """ Return the nth prime number.
    """
    pass


def gcd_iter(n, m):
    """ An iterative function that calculates the GCD of n and m.
    Note that there is a function from the fractions module,
    fractions.gcd(a, b), that computes this -- do not use this
    function, but replicate its behavior exactly (including for
    negative or 0 inputs). See its documentation here:
    https://docs.python.org/3/library/fractions.html
    """
    pass


def gcd_rec(n, m):
    """ A recursive function that calculates the GCD of n and m.
    """
    pass


def fib_iter(n):
    """ An iterative function that calculates the nth Fibonacci number.
    By convention we will say that the 1st Fibonacci number is 0
    and the 2nd Fibonacci number is 1.
    """
    pass


def fib_rec(n):
    """ A recursive function that calculates the nth Fibonacci number.
    """
    pass


def main():
    pass


if __name__ == "__main__":
    """ Runs main() if we run this file with 'python hw1.py'. """
    main()
